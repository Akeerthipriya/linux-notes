# Linux: Shell and Commands

## What is a Shell ?
1. shell is an application
1. protects the kernel from direct access
1. interface between kernel and the user or application (CLI: Command-line Interface)
1. performs syntax check, parameter expansions on user input
1. provides built-in commands
1. lets user run commands individually or as scripts 
1. supports interactive mode (CLI) and batch mode(scripting)

## Types of Shell

> **Bourne Again Shell** (`bash`) is the default shell used in Linux

1. Bourne Shell (sh)
1. Bourne Again Shell (bash)
1. C Shell (csh)
1. Korn Shell (ksh)
1. z Shell (zsh)